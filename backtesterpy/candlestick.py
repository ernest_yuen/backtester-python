import pandas as pd
import numpy as np


class Candlestick:

    def __init__(self, df, df_dayfirst=True):
        self._df = df
        self._df.index = pd.to_datetime(self._df.Date, dayfirst=df_dayfirst)


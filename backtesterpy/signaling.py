import pandas as pd
from sklearn.linear_model import LinearRegression


class Signaling:

    def __init__(self, df: pd.DataFrame, dependent_independent_col_name: list):

        dependent_var, independent_var = dependent_independent_col_name[0], dependent_independent_col_name[1]
        self._new_df = pd.DataFrame(0.0, index=df.index, columns=['dependent', 'independent'])
        self._new_df['dependent'] = df[dependent_var]
        self._new_df['independent'] = df[independent_var]
        pass

    def _fetch_slope_and_intercept(self, training_lookback_number: int):
        status_code = True
        if training_lookback_number > self._new_df.__len__():
            print(f'There is not enough data points.')
            status_code = False
            return status_code, None, None, None

        training_data = self._new_df.iloc[0:training_lookback_number]
        testing_data = self._new_df.iloc[training_lookback_number:]
        reg = LinearRegression()
        reg.fit(training_data['independent'], training_data['dependent'])
        slope, intercept = reg.coef_, reg.intercept_
        testing_data['theoretical_dependent_value'] = testing_data['independent'] * slope + intercept
        testing_data['difference'] = testing_data['dependent'] - testing_data['theoretical_dependent_value']

        return status_code, testing_data, slope, intercept
    
    def _leading_signal_trade(self, training_lookback_number: int, lookback_window_for_difference: int, para: dict,
                              transaction_cost, method='linear_regression'):
        # The Dependent gets the signal to lead the Independent.
        if para.get('name') is None or para.get('value') is None:
            print(f'The para is Invalid.')

        if para.get('name') == 'absolute_percentage' or para.get('name') == 'z_score':
            status_code, backtest_dataframe, slope, intercept = \
                self._fetch_slope_and_intercept(training_lookback_number=training_lookback_number)

            if status_code is False:
                return status_code, None

            else:
                backtest_dataframe[f'{lookback_window_for_difference}-day sum'] = backtest_dataframe['difference'].\
                    rolling(window=lookback_window_for_difference).sum()

                if para['name'] == 'absolute_percentage':
                    # Long Independent
                    backtest_dataframe['signal_long'] = \
                        backtest_dataframe[f'{lookback_window_for_difference}-day sum'] > para['value']

                    # Short Independent
                    backtest_dataframe['signal_short'] = \
                        backtest_dataframe[f'{lookback_window_for_difference}-day sum'] < -para['value']

                    backtest_dataframe['profit_and_loss'] = 0.0

                    for i in range(len(backtest_dataframe)-1):

                        if backtest_dataframe['signal_long'].iloc[i]:
                            profit_and_loss = backtest_dataframe['independent'].iloc[i+1]

                        elif backtest_dataframe['signal_short'].iloc[i]:
                            profit_and_loss = - backtest_dataframe['independent'].iloc[i+1]

                        else:
                            profit_and_loss = 0.0

                        backtest_dataframe['profit_and_loss'].iloc[i+1] = profit_and_loss

                if para['name'] == 'z_score':
                    pass

        else:
            return 0, None

    def _pair_trading(self, training_lookback_number: int, lookback_window: int, z_score_threshold: float,
                      transaction_cost, method='linear_regression'):
        status_code, backtest_dataframe, slope, intercept = \
            self._fetch_slope_and_intercept(training_lookback_number=training_lookback_number)

        if status_code is False:
            return status_code, None

        else:
            backtest_dataframe['dif_ma'] = backtest_dataframe['difference'].rolling(window=lookback_window).mean()
            backtest_dataframe['dif_sd'] = backtest_dataframe['difference'].rolling(window=lookback_window).std()

            # Short Independent Long Dependent
            backtest_dataframe['signal_long'] = (backtest_dataframe['difference'] - backtest_dataframe['dif_ma']) / \
                                                backtest_dataframe['dif_sd'] < -z_score_threshold

            # Short Dependent Long Independent
            backtest_dataframe['signal_short'] = (backtest_dataframe['difference'] - backtest_dataframe['dif_ma']) / \
                                                 backtest_dataframe['dif_sd'] > z_score_threshold

            backtest_dataframe['profit_and_loss'] = 0.0

            for i in range(len(backtest_dataframe)-1):

                if backtest_dataframe['signal_long'].iloc[i]:
                    profit_and_loss = backtest_dataframe['dependent'].iloc[i+1] * slope \
                                      - backtest_dataframe['independent'].iloc[i+1]

                elif backtest_dataframe['signal_short'].iloc[i]:
                    profit_and_loss = backtest_dataframe['independent'].iloc[i+1] \
                                      - backtest_dataframe['dependent'].iloc[i+1] * slope

                else:
                    profit_and_loss = 0.0

                backtest_dataframe['profit_and_loss'].iloc[i+1] = profit_and_loss

        return status_code, backtest_dataframe

    def leading_signal_trade_pnl(self, training_lookback_number: int, lookback_window: int, z_score_threshold: float,
                                 transaction_cost, method='linear_regression'):
        pass

    pass
